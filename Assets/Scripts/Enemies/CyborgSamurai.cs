﻿using System.Collections;
using UnityEngine;

public class CyborgSamurai : Enemy
{
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private float thrustForce;
    [SerializeField] private float antecipationTime;
    [SerializeField] private float attackCooldown;

    private bool canAttack = true;

    public override void Attack()
    {
        if (canAttack && isAlive)
        {
            StartCoroutine(JumpSlash());
            canAttack = false;
        }
    }

    IEnumerator JumpSlash()
    {
        yield return new WaitForSeconds(antecipationTime);

        base.Attack();

        yield return new WaitForSeconds(0.2f);

        enemyRigidbody.freezeRotation = true;
        var playerPos = Player.PlayerPosition;
        var thrustDirection = playerPos - (Vector2)transform.position;

        enemyRigidbody.AddForce(thrustDirection.normalized * thrustForce, ForceMode2D.Impulse);

        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
        enemyRigidbody.freezeRotation = false;
    }

    protected override void Die()
    {
        canAttack = false;
        base.Die();
    }
}
