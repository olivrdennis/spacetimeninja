﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class DamageReceiver : MonoBehaviour
{
    [SerializeField] private float life;
    [SerializeField] private float delayToDie;
    [SerializeField] private UnityEvent onTakeDamage;
    [SerializeField] private UnityEvent onDie;
    protected bool isAlive = true;

    public void TakeDamage(float damage)
    {
        onTakeDamage?.Invoke();
        life -= damage;
        if(life <= 0)
        {
            if (isAlive)
            {
                onDie?.Invoke();
                isAlive = false;
                Die();
            }
        }
    }

    protected virtual void Die()
    {
        Destroy(gameObject, delayToDie);
    }
}
