﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PolygonCollider2D))]
public class EnemyVision : MonoBehaviour
{
    [SerializeField] private Enemy enemy;

    [SerializeField] private UnityEvent onPlayerSpoted;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (player != null)
        {
            onPlayerSpoted?.Invoke();
            enemy.OnEnemySpoted();
            enemy.SetPatrol(false);
            enemy.Attack();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (player != null)
        {
            enemy.SetPatrol(true);
        }
    }

}
