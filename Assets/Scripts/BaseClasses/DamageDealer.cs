﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class DamageDealer : MonoBehaviour
{
    [SerializeField] private bool destroyWhenCollide;
    [SerializeField] private bool stuckOnTarget;
    [SerializeField] private float damage;
    protected Collider2D currentTrigger;
    protected Collision2D currentCollision;

    private void OnTriggerEnter2D(Collider2D other)
    {
        currentTrigger = other;

        OnTriggerCallback();

        var damageReceiver = other.gameObject.GetComponent<DamageReceiver>();
        if(damageReceiver != null)
        {
            damageReceiver.TakeDamage(damage);
            if (stuckOnTarget)
            {
                var rigidbody = gameObject.GetComponent<Rigidbody2D>();
                rigidbody.velocity = Vector2.zero;
                rigidbody.isKinematic = true;
                var shuriken = gameObject.GetComponent<Shuriken>();
                
                transform.SetParent(other.transform);
            }

            if (destroyWhenCollide) Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        currentCollision = collision;
        OnCollisionCallback();
    }

    protected virtual void OnTriggerCallback()
    {

    }

    protected virtual void OnCollisionCallback()
    {

    }
}
