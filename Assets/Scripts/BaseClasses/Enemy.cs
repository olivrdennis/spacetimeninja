﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : DamageReceiver
{
    public static Action OnEnemyDie;
    public static Action OnEnemySpawn;

    [Header("References")]
    [SerializeField] private Movement movement;
    [SerializeField] private Animator enemyAnimator;
    [SerializeField] private List<Transform> patrolWayPoints = new List<Transform>();

    [Header("Propeties")]
    [SerializeField] private float patrolStayTime;
    [SerializeField] private float secondsToRecal;

    [Header("Death")]
    [SerializeField] private GameObject deathParticle;

    private List<Vector3> currentPatrolPoints = new List<Vector3>();
    private bool canPatrol = true;
    private bool canSetBehaviour = true;
    private Vector2 initialPosition;

    IEnumerator patrolCoroutine;

    private void OnEnable()
    {
        patrolCoroutine = Patrol();
    }

    private void Start()
    {
        OnEnemySpawn?.Invoke();

        for (int i = 0; i < patrolWayPoints.Count; i++)
        {
            currentPatrolPoints.Add(patrolWayPoints[i].position);
        }

        initialPosition = transform.position;

        StartCoroutine(Patrol());
    }

    public void SetPatrol(bool patrol)
    {
        canPatrol = patrol;

        if (canSetBehaviour)
        {
            if (patrol)
            {
                StartCoroutine(patrolCoroutine);
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine(RecallTeleport());
            } 
        }
    }

    protected override void Die()
    {
        OnEnemyDie?.Invoke();
        movement.LockMovement();
        StartCoroutine(DelayedDeath());
    }

    IEnumerator DelayedDeath()
    {
        yield return new WaitForSeconds(1);
        var death = Instantiate(deathParticle) as GameObject;
        death.transform.position = new Vector2(transform.position.x, transform.position.y + 1);
        Destroy(gameObject);
    }

    public virtual void OnEnemySpoted()
    {

    }


    public virtual void Attack()
    {
        enemyAnimator.SetTrigger("Attack");
    }

    IEnumerator RecallTeleport()
    {
        canSetBehaviour = false;
        yield return new WaitForSeconds(secondsToRecal);
        enemyAnimator.SetTrigger("Teleport");
        yield return new WaitForSeconds(0.5f);
        transform.position = initialPosition;
        canPatrol = true;
        canSetBehaviour = true;
        StartCoroutine(Patrol());
    }

    IEnumerator Patrol()
    {
        enemyAnimator.SetBool("IsMoving", false);
        yield return new WaitForSeconds(1);

        while (canPatrol)
        {
            for (int i = 0; i < currentPatrolPoints.Count; i++)
            {
                var targetPos = currentPatrolPoints[i];
                while (Vector2.Distance(transform.position, targetPos) > 1f && canPatrol)
                {
                    if (targetPos.x > transform.position.x)
                    {
                        movement.Move(Direction.Right);
                    }
                    else
                    {
                        movement.Move(Direction.Left);
                    }
                    yield return null;
                }

                movement.targetRigidbody.velocity = Vector2.zero;
                enemyAnimator.SetBool("IsMoving", false);
                yield return new WaitForSeconds(patrolStayTime);
            }
        }
    }

}
