﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionAttack : MonoBehaviour
{
    public static Action<float> OnPlaceMark;

    [Header("References")]
    [SerializeField] private ControlInputType inputType;
    [SerializeField] private Transform fireDirectionObject;
    [SerializeField] private Transform aim;
    [SerializeField] private Movement movement;
    [SerializeField] private GameObject playerAnimationRender;
    [SerializeField] private Animator playerAnimator;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Rigidbody2D playerRigidbody;
    [SerializeField] private GameObject mark;
    [SerializeField] private Shuriken shuriken;
    [SerializeField] private Transform throwSpot;

    [Header("Properties")]
    [SerializeField] private float throwCooldown;
    [SerializeField] private float timeToLost;

    private List<GameObject> placedDirectionArrows = new List<GameObject>();
    private List<Vector2> marksPlaced = new List<Vector2>();
    private List<Vector2> fireDirections = new List<Vector2>();

    private bool canPlaceMarks = false;

    private float rightTrigger = 0;
    private bool canUseTrigger = true;

    private void OnEnable()
    {
        TimeStop.OnTimeStop += EnablePlaceMarks;
        TimeStop.OnTimeReturnNormal += FireShurikens;
    }

    private void OnDisable()
    {
        TimeStop.OnTimeStop -= EnablePlaceMarks;
        TimeStop.OnTimeReturnNormal -= FireShurikens;
    }

    private void Start()
    {
        aim.gameObject.SetActive(false);
    }

    void Update()
    {
        Aim();

        rightTrigger = Input.GetAxis("FireShuriken");

        if (Input.GetMouseButtonDown(0))
        {
            if (HasEnoughTime() && canPlaceMarks)
            {
                PlaceMark();
            } 
        }

        if(rightTrigger > 0 && canUseTrigger)
        {
            if (HasEnoughTime() && canPlaceMarks)
            {
                PlaceMark();
                StartCoroutine(JoystickTriggerCooldown());
            }
        }

    }

    private bool HasEnoughTime()
    {
        if (TimeStop.Instance.CurrentTime < TimeStop.Instance.TimeLimit)
            return true;
        else
            return false;
    }

    private void PlaceMark()
    {
        var directionArrow = Instantiate(mark) as GameObject;
        directionArrow.transform.position = fireDirectionObject.position;
        directionArrow.transform.rotation = fireDirectionObject.rotation;

        placedDirectionArrows.Add(directionArrow);

        marksPlaced.Add(fireDirectionObject.position);

        var newFireDirection = fireDirectionObject.position - throwSpot.position;
        fireDirections.Add(newFireDirection);

        OnPlaceMark?.Invoke(timeToLost);
    }

    private void EnablePlaceMarks()
    {
        aim.gameObject.SetActive(true);
        canPlaceMarks = true;
    }

    private void Aim()
    {
        float angle;

        switch (inputType)
        {
            case ControlInputType.Joystick:
                angle = Mathf.Atan2(Input.GetAxis("Vertical_R"), Input.GetAxis("Horizontal_R")) * Mathf.Rad2Deg - 90;
                aim.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);
                break;

            case ControlInputType.Keyboard:
                var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var direction = aim.position - mousePos;
                angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
                aim.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                break;
        }
    }

    private void FireShurikens()
    {
        aim.gameObject.SetActive(false);
        StartCoroutine(ThrowWithCooldown());
    }

    IEnumerator ThrowWithCooldown()
    {
        canPlaceMarks = false;

        movement.LockMovement();
        playerRigidbody.velocity = Vector2.zero;
        playerRigidbody.gravityScale = 0;
        playerRigidbody.isKinematic = true;

        for (int i = 0; i < marksPlaced.Count; i++)
        {
            if(marksPlaced[i].x > movement.transform.position.x)
            {
                playerAnimationRender.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else
            {
                playerAnimationRender.transform.rotation = Quaternion.Euler(0, 0, 0);
            }


            playerAnimator.SetTrigger("Attack");
            playerAnimator.SetFloat("AttackType", UnityEngine.Random.Range(1, 3));

            var newShuriken = Instantiate(shuriken) as Shuriken;
            newShuriken.transform.position = throwSpot.position;
            newShuriken.ThrowShuriken(fireDirections[i]);
            Destroy(placedDirectionArrows[i]);
            playerRigidbody.gravityScale = 0;
            playerRigidbody.velocity = Vector2.zero;

            playerAnimator.SetTrigger("InAir");
            yield return new WaitForSeconds(throwCooldown);
        }

        placedDirectionArrows.Clear();
        marksPlaced.Clear();
        fireDirections.Clear();

        playerAnimator.SetTrigger("InAir");
        //playerAnimator.SetTrigger("EndAttack");
        yield return new WaitForSeconds(0);
        playerRigidbody.isKinematic = false;
        movement.UnlockMovement();
    }

    IEnumerator JoystickTriggerCooldown()
    {
        canUseTrigger = false;
        yield return new WaitForSeconds(0.003f);
        canUseTrigger = true;
    }
}
