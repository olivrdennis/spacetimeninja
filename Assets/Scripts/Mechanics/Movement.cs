﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [Header("References")]
    [SerializeField] public Rigidbody2D targetRigidbody;
    [SerializeField] private Transform animationRender;
    [SerializeField] private Animator targetAnimator;
    [Header("Properties")]
    [SerializeField] private bool listenToPlayersInput;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float moveSpeed;

    [HideInInspector] public float HorizontalSpeed { get; set; }
    [HideInInspector] public float VerticalSpeed { get; set; } 

    private bool canMove = true;
    private bool moveRight = false;
    private bool moveLeft = false;

    private const string isMoving = "IsMoving";

    private void OnEnable()
    {
        TimeStop.OnTimeStop += LockMovement;
        TimeStop.OnTimeReturnNormal += UnlockMovement;
    }

    private void OnDisable()
    {
        TimeStop.OnTimeStop -= LockMovement;
        TimeStop.OnTimeReturnNormal -= UnlockMovement;
    }

    void FixedUpdate()
    {
        UpdateMovement();
    }

    private void Update()
    {
        if (!moveLeft && !moveRight && HorizontalSpeed == 0)
        {
            targetAnimator.SetBool(isMoving, false);
        }
    }

    public void Move(Direction direction)
    {
        targetAnimator.SetBool(isMoving, true);

        switch (direction)
        {
            case Direction.Left:
                moveLeft = true;
                break;

            case Direction.Right:
                moveRight = true;
                break;
        }
    }

    private void UpdateMovement()
    {
        #region Mouse and Keyboard
        if (canMove)
        {
            if (moveLeft)
            {
                animationRender.rotation = Quaternion.Euler(0, 0, 0);

                if (Mathf.Abs(targetRigidbody.velocity.x) < Mathf.Abs(maxSpeed))
                    targetRigidbody.AddForce(new Vector2(-moveSpeed, 0));

                moveLeft = false;
                return;
            }

            if (moveRight)
            {
                animationRender.rotation = Quaternion.Euler(0, 180, 0);

                if (Mathf.Abs(targetRigidbody.velocity.x) < Mathf.Abs(maxSpeed))
                    targetRigidbody.AddForce(new Vector2(moveSpeed, 0));

                moveRight = false;
                return;
            }
        }
        #endregion

        #region Joystick
        if (canMove)
        {
            targetRigidbody.AddForce(new Vector2(HorizontalSpeed * moveSpeed, 0));

            if(HorizontalSpeed > 0)
            {
                animationRender.rotation = Quaternion.Euler(0, 180, 0);
                targetAnimator.SetBool(isMoving, true);
            }

            if (HorizontalSpeed < 0)
            {
                animationRender.rotation = Quaternion.Euler(0, 0, 0);
                targetAnimator.SetBool(isMoving, true);
            }

        }
        #endregion
    }

    public void LockMovement()
    {
        targetRigidbody.velocity = Vector2.zero;
        canMove = false;
    }

    public void UnlockMovement()
    {
        canMove = true;
    }
}
