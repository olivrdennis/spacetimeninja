﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeStop : MonoBehaviour
{
    public static TimeStop Instance;

    public static Action OnTimeStop;
    public static Action OnTimeReturnNormal;

    [Header("References")]
    [SerializeField] private Image timeBar;
    [Header("Properties")]
    [SerializeField] private float timeLimit;
    public float TimeLimit {
        get { return timeLimit; }
        set { timeLimit = value; }
    }
    [SerializeField] private float timeScale;
    [SerializeField] private float burnSpeed;
    [SerializeField] private float rechargeSpeed;

    private bool canStopTime = true;
    private float currentTime;
    public float CurrentTime {
        get { return currentTime; }
        set { currentTime = value; }
    }

    private bool isTimeStopped = false;
    private float leftTrigger = 0;

    private void OnEnable()
    {
        SlowMotionAttack.OnPlaceMark += SubtractTime;
    }

    private void OnDisable()
    {
        SlowMotionAttack.OnPlaceMark -= SubtractTime;
    }

    private void Start()
    {
        if (Instance != null)
        {
            Destroy(Instance.gameObject);
            DontDestroyOnLoad(this.gameObject);
        }

        Instance = this;

        currentTime = timeLimit;
    }

    void Update()
    {
        leftTrigger = Input.GetAxis("StopTime");

        if (Input.GetMouseButton(1) || leftTrigger > 0)
        {
            if (canStopTime)
            {
                StartCoroutine(StopTime());
            }
        }
        else
        {
            Time.timeScale = 1f;
            StartCoroutine(Recharge());
        } 
    }

    IEnumerator StopTime()
    {
        isTimeStopped = true;
        OnTimeStop?.Invoke();
        Time.timeScale = timeScale;
        canStopTime = false;
        while (currentTime > 0)
        {
            yield return null;
            UpdateBar();
            currentTime -= Time.deltaTime * burnSpeed;
        }
        Time.timeScale = 1;
        StartCoroutine(Recharge());
        OnTimeReturnNormal?.Invoke();
    }

    IEnumerator Recharge()
    {
        while (currentTime < timeLimit)
        {
            UpdateBar();
            if (currentTime + rechargeSpeed >= timeLimit)
            {
                currentTime = timeLimit;
            }
            else
            {
                currentTime += rechargeSpeed;
            }
            yield return null;
        }
        canStopTime = true;
    }

    

    private void UpdateBar()
    {
        timeBar.fillAmount = currentTime / timeLimit;
    }

    private void SubtractTime(float amount)
    {
        currentTime -= amount;
    }
}
