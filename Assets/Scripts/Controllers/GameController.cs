﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private Animator UIAnimator;

    private int enemiesAlive;
    private int currentStageIndex;
    private Scene currentScene;

    private void OnEnable()
    {
        Enemy.OnEnemySpawn += AddAliveEnemy;
        Enemy.OnEnemyDie += RemoveDeadEnemy;
        Player.OnPlayerDie += RestartGame;
    }

    private void OnDisable()
    {
        Enemy.OnEnemySpawn -= AddAliveEnemy;
        Enemy.OnEnemyDie -= RemoveDeadEnemy;
        Player.OnPlayerDie -= RestartGame;
    }

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        currentStageIndex = 0;
    }

    private void RestartGame()
    {
        StartCoroutine(RestartGameCoroutine());
    }

    private void CheckVictory()
    {
        if (enemiesAlive <= 0)
        {
            StartCoroutine(Victory());
        }
    }

    IEnumerator RestartGameCoroutine()
    {
        UIAnimator.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    IEnumerator Victory()
    {
        yield return new WaitForSeconds(2);
        yield return TryNextLevel();
    }

    private void AddAliveEnemy()
    {
        enemiesAlive++;
        CheckVictory();
    }

    private void RemoveDeadEnemy()
    {
        enemiesAlive--;
        CheckVictory();
    }

    private IEnumerator TryNextLevel()
    {
        UIAnimator.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

    }
}
