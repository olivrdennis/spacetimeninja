﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Shuriken : DamageDealer
{
    [Header("References")]
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody2D shurikenRigidbody;
    [Header("Properties")]
    [SerializeField] private float lifeTime;
    [SerializeField] private float timeToLost;
    public float TimeToLost {
        get { return timeToLost; }
        set { timeToLost = value; }
    }

    [SerializeField] private float impulseForce;
    [SerializeField] private float impactForce;

    public void ThrowShuriken(Vector2 direction)
    {
        shurikenRigidbody.AddForce(direction.normalized * impulseForce, ForceMode2D.Impulse);
        Destroy(gameObject, lifeTime);
    }

    protected override void OnCollisionCallback()
    {
        animator.SetBool("IsSpinning", false);
        shurikenRigidbody.velocity = Vector2.zero;
        shurikenRigidbody.isKinematic = true;
    }

    protected override void OnTriggerCallback()
    {
        animator.SetBool("IsSpinning", false);
        shurikenRigidbody.velocity = Vector2.zero;
        //shurikenRigidbody.isKinematic = true;
    }

}
