﻿using System;
using System.Collections;
using UnityEngine;

public class Player : DamageReceiver
{
    public static Vector2 PlayerPosition;

    public static Action OnPlayerDie;

    [Header("References")]
    [SerializeField] private GroundChecker groundChecker;
    [SerializeField] private Movement movement;
    [SerializeField] private Rigidbody2D playerRididbody;
    [SerializeField] private Transform animationRender;
    [SerializeField] private Animator playerAnimator;

    [Header("Properties")]
    [SerializeField] private float airMoveSpeed;
    [SerializeField] private float jumpForce;

    private bool canJump = false;
   
    private Direction direction;

    private void OnEnable()
    {
        TimeStop.OnTimeStop += SetAirPose;
    }

    private void OnDisable()
    {
        TimeStop.OnTimeStop -= SetAirPose;
    }

    private void Update()
    {
        PlayerPosition = transform.position;

        if (Input.GetKey(KeyCode.D))
        {
            movement.Move(Direction.Right);
        }

        if (Input.GetKey(KeyCode.A))
        {
            movement.Move(Direction.Left);
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Jump"))
        {
            if (IsOnGround()) Jump();
        }

        if (IsOnGround())
        {
            playerAnimator.SetBool("IsOnGround", true);
        }
        else
        {
            playerAnimator.SetBool("IsOnGround", false);
        }

        movement.HorizontalSpeed = Input.GetAxis("Horizontal");
    }

    private bool IsOnGround()
    {
        if (groundChecker.IsOnGround)
        {
            playerRididbody.gravityScale = 3;
            return true;
        }
        else
        {
            playerRididbody.gravityScale = 12;
            return false;
        }
    }


    private void Jump()
    {
        if (isAlive)
        {
            playerAnimator.SetTrigger("Jump");
            playerRididbody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse); 
        }
    }

    private void SetAirPose()
    {
        playerAnimator.SetTrigger("InAir");
    }

    protected override void Die()
    {
        base.Die();

        StartCoroutine(SlowMotionDeath());
        
        movement.LockMovement();
    }

    IEnumerator SlowMotionDeath()
    {
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 1;
        OnPlayerDie?.Invoke();
    }
}

