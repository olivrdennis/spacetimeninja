﻿using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public bool IsOnGround;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            IsOnGround = true;
            Debug.Log("Is On Ground " + IsOnGround);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Ground"))
        {
            IsOnGround = false;
            Debug.Log("Is On Ground " + IsOnGround);
        }
    }
}
